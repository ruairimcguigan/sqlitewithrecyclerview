package demo.recyclerview.sqlite.com.sqlitewithrecyclerview.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import demo.recyclerview.sqlite.com.sqlitewithrecyclerview.R;
import demo.recyclerview.sqlite.com.sqlitewithrecyclerview.listeners.RowDeleteListener;
import demo.recyclerview.sqlite.com.sqlitewithrecyclerview.model.User;

/**
 * Created by rmcguigan on 23/03/16.
 */
public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {

    Context mContext;
    List<User> mUserList = new ArrayList<>();
    LayoutInflater inflater;
    RowDeleteListener mListener;

    public ListAdapter(Context context, List<User> userList){
        mContext = context;
        mUserList = userList;
        mListener = (RowDeleteListener) context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View convertView = inflater.inflate(R.layout.list_user_item, parent, false);
        ListViewHolder viewHolder = new ListViewHolder(convertView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int position) {

        holder.iv_delete.setTag(position);
        holder.tv_name.setText(mUserList.get(position).getUsername());

        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.userToDelete(mUserList.get((Integer) v.getTag()).getUsername());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mUserList.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder{

        TextView tv_name;
        ImageView iv_delete;
        public ListViewHolder(View itemView) {
            super(itemView);

            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            iv_delete= (ImageView) itemView.findViewById(R.id.iv_delete);
        }
    }
}
