package demo.recyclerview.sqlite.com.sqlitewithrecyclerview.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import demo.recyclerview.sqlite.com.sqlitewithrecyclerview.model.User;

/**
 * Created by rmcguigan on 23/03/16.
 */
public class DbOpenHelper extends SQLiteOpenHelper {

    private static DbOpenHelper mDbOpenHelper;
    private static final String LOGTAG = "SQLOpenHelper";

    private static final String TAG = "DbHelper";
    // Database Info
    private static final String DATABASE_NAME = "UserDatabase";
    private static final int DATABASE_VERSION = 1;

    //Table Names
    private static final String TABLE_USERS = "userdetail";


    //userdetail Table Columns
    private static final String USER_ID = "_id";
    private static final String NAME = "name";
    private static final String PHONE_NUMBER = "number";
    private static final String CITY = "place";
    private static final String COLLEGE = "college";

    //SQL to create table
    private static final String CREATE_USER_TABLE =
            "CREATE TABLE " + TABLE_USERS + " (" +
                    USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    NAME + " TEXT, " +
                    COLLEGE + " TEXT, " +
                    CITY + " TEXT, " +
                    PHONE_NUMBER + " TEXT " +
                    ")";

    /**
     * @param context
     * @return
     *
     * 'Synchronised - to prevent potential thread interference and memory consistency errors:
     * as this newInstance method will potetially be visible to more than one thread,
     * all reads or writes to that object's variables are done through synchronized methods. - thus
     * maintaining data integrity
     *
     */
    public static synchronized DbOpenHelper getInstance(Context context) {
        // Using the application context, which will ensure that an Activity's context is
        // not accidentally leaked

        if (mDbOpenHelper == null) {
            mDbOpenHelper = new DbOpenHelper(context.getApplicationContext());
        }
        return mDbOpenHelper;
    }

    /**
     * Constructor is private to prevent direct instantiation.
     * Make a call to the static method "getInstance()" instead.
     */
    public DbOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_USER_TABLE);
        Log.i(LOGTAG, "Table has been created");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            // Simplest implementation is to drop all old tables and recreate them
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
            onCreate(db);
        }
    }

    /**
     * Insert user into db
     */
    public void insertUser(User user){

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        try{
            ContentValues values = new ContentValues();
            values.put(USER_ID, user.getUserID());
            values.put(NAME, user.getUsername());
            values.put(PHONE_NUMBER, user.getPhoneNumber());
            values.put(CITY, user.getCity());
            values.put(COLLEGE, user.getCollege());

            db.insertOrThrow(TABLE_USERS, null, values);
            db.setTransactionSuccessful();
        }catch (SQLException e){
            e.printStackTrace();
            Log.e(TAG, "Error trying to insert to database");
        // always
        }finally {
            db.endTransaction();
        }
    }

    /**
     * Delete single row from UserTable
     */
    public void deleteUser(String name){

        SQLiteDatabase db = getWritableDatabase();

        try{
            db.beginTransaction();
            db.execSQL("delete from " + TABLE_USERS + " where name ='" + name + "'");
            db.setTransactionSuccessful();
        }catch (SQLException e){
            Log.d(TAG, "Error while trying to delete user details");
        }finally {
            db.endTransaction();
        }
    }

    /**
     * Insert user into db
     */
    public List<User> fetchAllUsers(){

        List<User> userList = new ArrayList<>();

        String USER_DETAIL_SELECT_QUERY = "SELECT * FROM " + TABLE_USERS;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(USER_DETAIL_SELECT_QUERY, null);

        try{
            if(cursor.moveToFirst()){
                do{
                    User user = new User();
                    user.setUserID(cursor.getString(cursor.getColumnIndex(USER_ID)));
                    user.setUsername(cursor.getString(cursor.getColumnIndex(NAME)));
                    user.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
                    user.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
                    user.setCollege(cursor.getString(cursor.getColumnIndex(COLLEGE)));

                    userList.add(user);
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            Log.d(TAG, "Error while trying to get posts from database");
        }finally {
            if(cursor != null && !cursor.isClosed()){
                cursor.close();
            }
        }
        return userList;
    }

}
