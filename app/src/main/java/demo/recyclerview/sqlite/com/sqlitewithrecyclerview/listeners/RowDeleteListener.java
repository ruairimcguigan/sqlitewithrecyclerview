package demo.recyclerview.sqlite.com.sqlitewithrecyclerview.listeners;

/**
 * Created by rmcguigan on 23/03/16.
 */
public interface RowDeleteListener  {

    void userToDelete(String name);
}
