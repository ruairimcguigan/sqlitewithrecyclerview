package demo.recyclerview.sqlite.com.sqlitewithrecyclerview.model;

import java.io.Serializable;

/**
 * To serialize an object means to convert its state to a byte stream so that the byte stream can
 * be reverted back into a copy of the object. A Java object is serializable if its class or any of
 * its superclasses implements either the java.io.Serializable interface or its subinterface,
 * java.io.Externalizable.
 */
public class User implements Serializable {

    String username;
    String userID;
    String college;
    String city;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    String phoneNumber;
}
