package demo.recyclerview.sqlite.com.sqlitewithrecyclerview.view.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import demo.recyclerview.sqlite.com.sqlitewithrecyclerview.R;
import demo.recyclerview.sqlite.com.sqlitewithrecyclerview.adapter.ListAdapter;
import demo.recyclerview.sqlite.com.sqlitewithrecyclerview.db.DbOpenHelper;
import demo.recyclerview.sqlite.com.sqlitewithrecyclerview.listeners.RowDeleteListener;

/**
 * Created by rmcguigan on 23/03/16.
 */
public class ListActivity extends Activity implements RowDeleteListener{

    RecyclerView recyclerView;
    DbOpenHelper dbOpenHelper;
    ListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recyclerview);

        dbOpenHelper = DbOpenHelper.getInstance(getApplicationContext());

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        adapter = new ListAdapter(this, dbOpenHelper.fetchAllUsers());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void userToDelete(String name) {
        dbOpenHelper.deleteUser(name);

        adapter = new ListAdapter(this, dbOpenHelper.fetchAllUsers());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
