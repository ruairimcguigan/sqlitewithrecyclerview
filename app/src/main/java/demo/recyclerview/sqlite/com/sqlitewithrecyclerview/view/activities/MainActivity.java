package demo.recyclerview.sqlite.com.sqlitewithrecyclerview.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import demo.recyclerview.sqlite.com.sqlitewithrecyclerview.R;
import demo.recyclerview.sqlite.com.sqlitewithrecyclerview.db.DbOpenHelper;
import demo.recyclerview.sqlite.com.sqlitewithrecyclerview.model.User;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText mEditID, mEditName, mEditPhone, mEditCity, mEditCollege;
    Button mMoveButton;
    DbOpenHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initToolbar();
        initClickListener();
        initFAB();

        dbHelper = DbOpenHelper.getInstance(getApplicationContext());
    }

    private void initClickListener() {
        mMoveButton.setOnClickListener(this);
    }

    private void initViews() {
        mEditID = (EditText) findViewById(R.id.userID);
        mEditName = (EditText) findViewById(R.id.username);
        mEditPhone = (EditText) findViewById(R.id.phoneNumber);
        mEditCity = (EditText) findViewById(R.id.city);
        mEditCollege = (EditText) findViewById(R.id.college);
        mMoveButton = (Button) findViewById(R.id.button_next);
    }

    private void initFAB() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_next) {

            User user = new User();

            /**
             * Basic validation
             */
            if (mEditID.getText().toString().isEmpty()) {
                mEditID.setError(getString(R.string.required_field));
                mEditID.requestFocus();
            } else {
                user.setUserID(mEditID.getText().toString());
            }
            if (mEditName.getText().toString().isEmpty()) {
                mEditName.setError(getString(R.string.required_field));
                mEditName.requestFocus();
            } else {
                user.setUsername(mEditName.getText().toString());
            }
            if (mEditPhone.getText().toString().isEmpty()) {
                mEditPhone.setError(getString(R.string.required_field));
                mEditPhone.requestFocus();
            } else {
                user.setPhoneNumber(mEditPhone.getText().toString());
            }
            if (mEditCity.getText().toString().isEmpty()) {
                mEditCity.setError(getString(R.string.required_field));
                mEditCity.requestFocus();
            } else {
                user.setCity(mEditCity.getText().toString());
            }
            if (mEditCollege.getText().toString().isEmpty()) {
                mEditCollege.setError(getString(R.string.required_field));
                mEditCollege.requestFocus();
            } else {
                user.setCity(mEditCollege.getText().toString());
            }

            dbHelper.insertUser(user);

            //TODO - implement check to see that no fields are empty before proceeding
            Intent intent = new Intent(this, ListActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
